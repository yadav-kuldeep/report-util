"use strict";

const fs = require("fs");
const inquirer = require("inquirer");
const fetch = require("node-fetch");
const _ = require("lodash");

let questions = [
  {
    type: "input",
    name: "username",
    message: "Enter company username : ",
    default: "fruitsco-dev@noosyntech.com",
  },
  {
    type: "password",
    name: "password",
    message: "Enter company user password : ",
    default: "123456",
  },
  {
    type: "input",
    name: "orderNo",
    message: "Enter order no : ",
  },
  {
    type: "confirm",
    name: "isPacklist",
    message: "Packlist? : ",
    default: false,
  },
  {
    type: "rawlist",
    name: "type",
    message: "Which type?",
    choices: ["xls", "pdf"],
    default: 0,
  },
  {
    type: "rawlist",
    name: "typeId",
    message: "Commercial invoice (Select 1) or simple (Select 2)?",
    choices: [2, 1],
    default: 2,
  },
];

const generateReport = (token, orderNo, type, isPacklist, typeId) => {
  const resource = isPacklist ? "packList" : "invoice";
  const url = `https://proxy.frood.tech/v1/${resource}/${orderNo}?typeId=${typeId}&fileType=${type}`;
  console.log(url);
  fetch(url, {
    headers: {
      "X-TOKEN": token,
      accept: "application/json",
      "content-type": "application/json",
    },
  })
    .then(res => {
      if (res.status !== 200) {
        throw new Error("Something wrong with request, ask your developer!!!");
      }
      return res.json();
    })
    .then(data => {
      //console.log(data);
      let buf = Buffer.from(data[0].reportData, "base64");
      fs.writeFileSync(`${orderNo}-${resource}.${type}`, buf);
    });
};

inquirer.prompt(questions).then(answers => {
  const { username, password, orderNo, isPacklist, type, typeId } = answers;
  let url = `https://proxy.frood.tech/v1/login`;
  const getToken = () => {
    return fetch(url, {
      method: "POST",
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
      body: JSON.stringify({ username, password }),
    });
  };

  getToken()
    .then(res => {
      if (res.status !== 200) {
        throw new Error("Wrong credentials!!!");
      }
      return res.json();
    })
    .then(data => {
      let token;
      if (data.companies.length > 1) {
        const companies = _.mapKeys(data.companies, "name");
        const compChoices = _.reduce(
          companies,
          (result, comp) => {
            return result.concat(comp.name);
          },
          []
        );
        let question = [
          {
            type: "rawlist",
            name: "company",
            message: "Choose company : ",
            choices: compChoices,
          },
        ];

        inquirer.prompt(question).then(answers => {
          const { company } = answers;
          token = companies[compChoices[company - 1]];
          generateReport(token, orderNo, type, isPacklist, typeId);
        });
      } else {
        token = data.companies[0].token;
        generateReport(token, orderNo, type, isPacklist, typeId);
      }
    })
    .catch(error => {
      console.log(error.message);
    });
});
